from flask import Flask, render_template, request, redirect, url_for, jsonify, Response
from threading import Thread

import mediapipe as mp

import cv2
import time
import json
import requests

import blinking
import drawcontours
import rangeshsv

app = Flask(__name__)

blinking.blink_mode('normal')
# Inicializar modelo de detección de manos
mp_drawing = mp.solutions.drawing_utils
mphands = mp.solutions.hands
hands = mphands.Hands()

# Establece el rango de colores desde el archivo JSON
with open("color_values.json", "r") as file:
    rangos_color = json.load(file)


# Ajustes de la cámara
cam = 0
fourcc_type = 'mp4v'
fourcc = cv2.VideoWriter_fourcc(*fourcc_type)
codec = fourcc  # MJPG
width, height = 800, 600
fps = 30
# brightness, contrast, saturation, focus, gamma, exposure, sharpness = -10, 30, -0.4, 10.0, 3.0, 0.3, 0.1
brightness, contrast, saturation, focus, gamma, exposure, sharpness = -10, -0.5, -0.5, 10.0, 3.0, 0.1, 0.1
cap = cv2.VideoCapture(cam)
cap.set(cv2.CAP_PROP_FOURCC, codec)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
cap.set(cv2.CAP_PROP_FPS, fps)
cap.set(cv2.CAP_PROP_BRIGHTNESS, brightness)
cap.set(cv2.CAP_PROP_CONTRAST, contrast)
cap.set(cv2.CAP_PROP_SATURATION, saturation)
cap.set(cv2.CAP_PROP_FOCUS, focus)
cap.set(cv2.CAP_PROP_GAMMA, gamma)
cap.set(cv2.CAP_PROP_SHARPNESS, sharpness)
cap.set(cv2.CAP_PROP_EXPOSURE, exposure)

# Inicializar el conteo de FPS
fps_disp = 0

# Inicializar el recuento de objetos para cada color. Esto es para solo contar
color_count = {"red": 0, "yellow": 0, "green": 0, "blue": 0}

# Inicializar el recuento de objetos para cada color. Esto es para contar y acumular
objects_count = {"red": 0, "yellow": 0, "green": 0, "blue": 0}

# Define los parámetros de la línea vertical.
line_x = 320  # Coordenada X de la línea vertical


def gen_frames():
    global fps_disp, color_count, detected, objects_count

    # Inicializar el seguimiento de los objetos detectados
    detected_objects = {
        "red": [],
        "yellow": [],
        "green": [],
        "blue": []
    }

    # Prueba de FPS
    start_time = time.time()
    display_time = 10
    fc = 0
    p_fps = 0

    while True:
        ret, frame = cap.read()
        if frame is None:
            break

        
        fc += 1
        TIME = time.time() - start_time
        if (TIME) >= display_time:
            p_fps = fc / (TIME)
            fc = 0
            start_time = time.time()
        fps_disp = round(p_fps, 2)
        
        frame = cv2.flip(frame, 0) # voltear la camara de forma vertical

        # Procesar la 
        frame = cv2.cvtColor(cv2.flip(frame, 1), cv2.COLOR_RGB2BGR)
        results = hands.process(frame)

        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        if results.multi_hand_landmarks:
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    frame,
                    hand_landmarks, mphands.HAND_CONNECTIONS)
            # Si se detecta manos, se activa la luz roja y la sirena del andon
            blinking.blink_mode('alert')

        

        contourscolor = rangeshsv.hsv(frame, rangos_color)
        red = drawcontours.draw(frame, contourscolor["red"], (0, 0, 255))
        yellow = drawcontours.draw(
            frame, contourscolor["yellow"], (0, 255, 255))
        green = drawcontours.draw(frame, contourscolor["green"], (0, 255, 0))
        blue = drawcontours.draw(frame, contourscolor["blue"], (255, 0, 0))

        tracked_objects = {}
        # Ciclo que recorre cada contorno y comprueba si cruza la línea vertical.
        for color, contourscolor in zip(["red", "yellow", "green", "blue"], [red, yellow, green, blue]):
            for i, c in enumerate(contourscolor):
                area = cv2.contourArea(c)
                if area > 8000:
                    # Calcula el centro del objeto.
                    M = cv2.moments(c)
                    if M["m00"] != 0:
                        x = int(M["m10"] / M["m00"])
                        y = int(M['m01'] / M['m00'])

                        # Comprueba si el objeto cruza la línea vertical.
                    if abs(x - line_x) < 5:
                        tracked_objects[i] = True
                        id_object = tracked_objects[i]
                        if id_object is True:
                            color_count[color] += 1
                            objects_count[color] += 1
                            # Resetea el conteo del endpoint
                        color_count = {"red": 0, "yellow": 0, "green": 0, "blue": 0}
                        
                    cv2.putText(frame, '{}'.format(objects_count[color]), (x + 10, y), cv2.FONT_HERSHEY_SIMPLEX, 0.55, (73, 0, 0), 2, cv2.LINE_AA)
                        
                    cv2.line(frame, (line_x, 0), (line_x, height), (0, 255, 0), 10)

        # Dibuja la linea vertical en el centro de la imagen
        cv2.line(frame, (line_x, 0), (line_x, height), (0, 0, 255), 3)

        # Reglas de negocio: parada de linea y oscio
        detected = color_count["red"] + color_count["yellow"] + \
            color_count["green"] + color_count["blue"]
        if detected == 0:
            # Si deja de contar se activa la luz amarilla del andon
            blinking.blink_mode('warning')
        else:
            # Se vuelve a activar la luz verde si inicia el conteo
            blinking.blink_mode('normal')
            #Thread(target=send_data).start()

        

        buffer = cv2.imencode('.jpg', frame)[1]
        if not ret:
            continue
        frame_bytes = buffer.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')

# Función para enviar datos a un endpoint


def send_data():
    endpoint = {
        "red": color_count["red"],
        "yellow": color_count["yellow"],
        "green": color_count["green"],
        "blue": color_count["blue"],
        "date": time.strftime("%Y-%m-%d"),
        "hour": time.strftime("%H"),
        "minutes": time.strftime("%M"),
        "secunds": time.strftime("%S")
    }
    endpoint_url = "https://2dw4l.wiremockapi.cloud/estera"
    response = requests.post(endpoint_url, json=endpoint)

    if response.status_code == 200:
        # Imprimir los datos que esta enviando
        print("Sending data:", endpoint)
        print(f"Data sent successfully.")
    else:
        print(f"Failed to send data.")


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        new_ranges = {}
        for color in rangos_color:
            new_ranges[color] = {
                'HUE': [int(request.form[f'{color}_h_min']), int(request.form[f'{color}_h_max'])],
                'SAT': [int(request.form[f'{color}_s_min']), int(request.form[f'{color}_s_max'])],
                'VALUE': [int(request.form[f'{color}_v_min']), int(request.form[f'{color}_v_max'])]
            }

        # Actualiza los valores de rango de color
        rangos_color.update(new_ranges)

        # Guardar en archivo JSON
        with open('color_values.json', 'w') as file:
            json.dump(rangos_color, file)

        return redirect(url_for('index'))
    return render_template('index.html', rangos_color=rangos_color)


# Ruta para mostar información de conteo


@app.route('/get_data')
def get_data():
    total_red = objects_count["red"]
    total_yellow = objects_count["yellow"]
    total_green = objects_count["green"]
    total_blue = objects_count["blue"]
    total = total_red + total_yellow + total_green + total_blue
    fecha = time.strftime("%d/%m/%Y")
    hora = time.strftime("%H")
    minutos = time.strftime("%M")
    segundos = time.strftime("%S")
    fps = fps_disp

    return jsonify({
        "total_red": total_red,
        "total_yellow": total_yellow,
        "total_green": total_green,
        "total_blue": total_blue,
        "total": total,
        "fecha": fecha,
        "hora": hora,
        "minutos": minutos,
        "segundos": segundos,
        "fps": fps
    })


# Ruta para restear el conteo

@app.route('/reset', methods=["GET"])
def reset_count():
    objects_count["red"] = 0
    objects_count["yellow"] = 0
    objects_count["green"] = 0
    objects_count["blue"] = 0
    return redirect('/')

    
# Ruta para mostrar la inferencia


@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == "__main__":
    video_thread = Thread(target=gen_frames)
    video_thread.start()
    app.run(host="0.0.0.0", port=8000, debug=False)
    video_thread.join()
    blinking.blink_mode('finish')

