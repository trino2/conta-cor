from multiprocessing import Process
import Jetson.GPIO as GPIO
import glob
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)

MOTOR = 7
GREEN = 11
YELLOW = 12
RED = 13

def blink_normal():
    try:
        while True:
            GPIO.output(MOTOR, False)
            GPIO.output(GREEN, False)
            GPIO.output(RED, True)
            GPIO.output(YELLOW, True)
    finally:
        GPIO.cleanup()



def blink_warning():
    try:
        while True:
            time.sleep(20)
            GPIO.output(MOTOR, False)
            GPIO.output(YELLOW, False)
            GPIO.output(RED, True)
            GPIO.output(GREEN, True)
        
    finally:
        GPIO.cleanup()



def blink_alert():
    try:
        while True:
            GPIO.output(RED, False)
            GPIO.output(MOTOR, True)
            GPIO.output(YELLOW, True)
            GPIO.output(GREEN, True)

    finally:
        GPIO.cleanup()

def blink_finish():
    try:
        while True:
            GPIO.output(RED, True)
            GPIO.output(MOTOR, True)
            GPIO.output(YELLOW, True)
            GPIO.output(GREEN, True)
    finally:
        GPIO.cleanup()



t_normal = Process(target=blink_normal)
t_warning = Process(target=blink_warning)
t_alert = Process(target=blink_alert)
t_finish = Process(target=blink_finish)

def blink_mode(mode):
    global t_normal, t_warning, t_alert, t_finish

    alive_normal = t_normal.is_alive()
    alive_warning = t_warning.is_alive()
    alive_alert = t_alert.is_alive()
    alive_finish = t_finish.is_alive()

    if mode == 'normal':
        if alive_warning == True:
            t_warning.terminate()
        
        if alive_alert == True:
            t_alert.terminate()

        if alive_normal == False:
            t_normal = Process(target=blink_normal)
            t_normal.start()
            
    if mode == 'warning':
        if alive_alert == True:
            t_alert.terminate()
        
        if alive_normal == True:
            t_normal.terminate()
        
        if alive_warning == False:
            t_warning = Process(target=blink_warning)
            t_warning.start()

    if mode == 'alert':
        if alive_normal == True:
            t_normal.terminate()
        
        if alive_warning == True:
            t_warning.terminate()
        
        if alive_alert == False:
            t_alert = Process(target=blink_alert)
            t_alert.start()

    if mode == 'Finish':
        if alive_normal == True:
            t_normal.terminate()
        
        if alive_warning == True:
            t_warning.terminate()

        if alive_alert == True:
            t_alert.terminate()
        
        if alive_finish == False:
            t_finish = Process(target=blink_finish)
            t_finish.start()
