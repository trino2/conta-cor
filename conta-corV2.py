import cv2
import numpy as np
import json
import time


font = cv2.FONT_HERSHEY_SIMPLEX

# Function to draw contours and calculate centroids within the ROI


def draw(frame, mask, color, roi_vertices):
    contours, _ = cv2.findContours(
        mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    centroids_in_roi = []

    for c in contours:
        area = cv2.contourArea(c)
        if area > 1000:
            M = cv2.moments(c)
            if M["m00"] == 0:
                M["m00"] = 1
            x = int(M["m10"] / M["m00"])
            y = int(M['m01'] / M['m00'])

            if cv2.pointPolygonTest(roi_vertices, (x, y), False) == 1:
                centroids_in_roi.append((x, y))

            newContour = cv2.convexHull(c)
            cv2.circle(frame, (x, y), 3, color, -1)
            cv2.putText(frame, '{},{}'.format(x, y), (x + 10, y),
                        font, 0.75, color, 1, cv2.LINE_AA)
            cv2.drawContours(frame, [newContour], 0, color, 3)

    return centroids_in_roi

# Empty function for creating trackbars.


def empty(a):
    pass

# Function to adjust the color range using trackbars and save to JSON file


def set_color_range():
    cv2.namedWindow("HSV")
    cv2.resizeWindow("HSV", 240, 620)

    # Initial values ​​for color ranges (red, yellow, green, blue)
    initial_values = {
        "red": [166, 90, 0, 179, 255, 255],
        "yellow": [29, 62, 79, 36, 255, 255],
        "green": [80, 90, 67, 103, 200, 255],
        "blue": [108, 114, 120, 144, 255, 255],
    }

    # Crearqq una trackbar para cada color y canal de color (H, S, V)
    for color, values in initial_values.items():
        for i, channel_name in enumerate(["HUE", "SAT", "VALUE"]):
            cv2.createTrackbar(f"{color} {channel_name} Min",
                               "HSV", values[i], 179 if i == 0 else 255, empty)
            cv2.createTrackbar(f"{color} {channel_name} Max", "HSV",
                               values[i + 3], 179 if i == 0 else 255, empty)

    # Read values ​​from JSON file if they exist
    try:
        with open("color_values.json", "r") as file:
            saved_values = json.load(file)
            for color, values in saved_values.items():
                for i, channel_name in enumerate(["HUE", "SAT", "VALUE"]):
                    cv2.setTrackbarPos(
                        f"{color} {channel_name} Min", "HSV", values[channel_name][0])
                    cv2.setTrackbarPos(
                        f"{color} {channel_name} Max", "HSV", values[channel_name][1])
    except FileNotFoundError:
        pass


# Function to save current values ​​to a JSON file
def save_values_in_json():
    values = {}
    for color in ["red", "yellow", "green", "blue"]:
        values[color] = {
            "HUE": [cv2.getTrackbarPos(f"{color} HUE Min", "HSV"), cv2.getTrackbarPos(f"{color} HUE Max", "HSV")],
            "SAT": [cv2.getTrackbarPos(f"{color} SAT Min", "HSV"), cv2.getTrackbarPos(f"{color} SAT Max", "HSV")],
            "VALUE": [cv2.getTrackbarPos(f"{color} VALUE Min", "HSV"), cv2.getTrackbarPos(f"{color} VALUE Max", "HSV")],
        }
    with open("color_values.json", "w") as file:
        json.dump(values, file, indent=4)

# Function to read the values ​​from the JSON file and load them into the trackbars


def read_values_from_json():
    try:
        with open("color_values.json", "r") as file:
            saved_values = json.load(file)
            for color, values in saved_values.items():
                for channel_name in ["HUE", "SAT", "VALUE"]:
                    cv2.setTrackbarPos(
                        f"{color} {channel_name} Min", "HSV", values[channel_name][0])
                    cv2.setTrackbarPos(
                        f"{color} {channel_name} Max", "HSV", values[channel_name][1])
    except FileNotFoundError:
        pass


# camera settings
cam = 0
width, height = 640, 480
fps = 60
#brightness, contrast, saturation, focus = 5, 130, 112, 50
brightness, contrast, saturation, focus = 0, 50, 85, 500



cap = cv2.VideoCapture(cam, cv2.CAP_V4L2)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
cap.set(cv2.CAP_PROP_FPS, fps)
cap.set(cv2.CAP_PROP_BRIGHTNESS, brightness)
cap.set(cv2.CAP_PROP_CONTRAST, contrast)
cap.set(cv2.CAP_PROP_SATURATION, saturation)
cap.set(cv2.CAP_PROP_FOCUS, focus)



def main():
    # Configuring the color range using trackbars
    set_color_range()
    # FPS Teste
    start_time = time.time()
    display_time = 10
    fc = 0
    p_fps = 0

    # Define the coordinates of the vertices of the ROI polygon
    roi_vertices = np.array([[338, 15],
                                [338, 450],
                                [343, 450],
                                [343, 15]], np.int32)

    # Initialize counters and lists to keep track of individual detections
    red_detections = []
    yellow_detections = []
    green_detections = []
    blue_detections = []

    while True:
        ret, frame = cap.read()

        # Check if a frame was successfully captured
        if not ret:
            break
        fc += 1

        TIME = time.time() - start_time

        if (TIME) >= display_time:
            p_fps = fc / (TIME)
            fc = 0
            start_time = time.time()

        fps_disp = "FPS: "+str(p_fps)[:5]

        # Add contador de frames
        frame = cv2.putText(frame, fps_disp, (15, 700),
                            font, 0.7, (255, 255, 255), 2)

        frameHsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Get the minimum and maximum values ​​of the color ranges
        red_h_min = cv2.getTrackbarPos("red HUE Min", "HSV")
        red_h_max = cv2.getTrackbarPos("red HUE Max", "HSV")
        red_s_min = cv2.getTrackbarPos("red SAT Min", "HSV")
        red_s_max = cv2.getTrackbarPos("red SAT Max", "HSV")
        red_v_min = cv2.getTrackbarPos("red VALUE Min", "HSV")
        red_v_max = cv2.getTrackbarPos("red VALUE Max", "HSV")

        yellow_h_min = cv2.getTrackbarPos("yellow HUE Min", "HSV")
        yellow_h_max = cv2.getTrackbarPos("yellow HUE Max", "HSV")
        yellow_s_min = cv2.getTrackbarPos("yellow SAT Min", "HSV")
        yellow_s_max = cv2.getTrackbarPos("yellow SAT Max", "HSV")
        yellow_v_min = cv2.getTrackbarPos("yellow VALUE Min", "HSV")
        yellow_v_max = cv2.getTrackbarPos("yellow VALUE Max", "HSV")

        green_h_min = cv2.getTrackbarPos("green HUE Min", "HSV")
        green_h_max = cv2.getTrackbarPos("green HUE Max", "HSV")
        green_s_min = cv2.getTrackbarPos("green SAT Min", "HSV")
        green_s_max = cv2.getTrackbarPos("green SAT Max", "HSV")
        green_v_min = cv2.getTrackbarPos("green VALUE Min", "HSV")
        green_v_max = cv2.getTrackbarPos("green VALUE Max", "HSV")

        blue_h_min = cv2.getTrackbarPos("blue HUE Min", "HSV")
        blue_h_max = cv2.getTrackbarPos("blue HUE Max", "HSV")
        blue_s_min = cv2.getTrackbarPos("blue SAT Min", "HSV")
        blue_s_max = cv2.getTrackbarPos("blue SAT Max", "HSV")
        blue_v_min = cv2.getTrackbarPos("blue VALUE Min", "HSV")
        blue_v_max = cv2.getTrackbarPos("blue VALUE Max", "HSV")

        # Define the color limits for blue in the HSV color space
        red_low = np.array(
            [red_h_min, red_s_min, red_v_min]).astype(np.uint8)
        red_high = np.array(
            [red_h_max, red_s_max, red_v_max]).astype(np.uint8)

        yellow_low = np.array(
            [yellow_h_min, yellow_s_min, yellow_v_min]).astype(np.uint8)
        yellow_high = np.array(
            [yellow_h_max, yellow_s_max, yellow_v_max]).astype(np.uint8)

        green_low = np.array(
            [green_h_min, green_s_min, green_v_min]).astype(np.uint8)
        green_high = np.array(
            [green_h_max, green_s_max, green_v_max]).astype(np.uint8)

        blue_low = np.array(
            [blue_h_min, blue_s_min, blue_v_min]).astype(np.uint8)
        blue_high = np.array(
            [blue_h_max, blue_s_max, blue_v_max]).astype(np.uint8)

        # create the mask
        maskRed = cv2.inRange(frameHsv, red_low, red_high)
        maskYellow = cv2.inRange(frameHsv, yellow_low, yellow_high)
        maskGreen = cv2.inRange(frameHsv, green_low, green_high)
        maskBlue = cv2.inRange(frameHsv, blue_low, blue_high)

        # Call the draw() function with the frame argument
        centroids_red = draw(frame, maskRed, (0, 0, 255), roi_vertices)
        centroids_yellow = draw(
            frame, maskYellow, (0, 255, 255), roi_vertices)
        centroids_green = draw(frame, maskGreen, (0, 255, 0), roi_vertices)
        centroids_blue = draw(frame, maskBlue, (255, 0, 0), roi_vertices)

        # Draw the ROI polygon on the frame
        cv2.polylines(frame, [roi_vertices], isClosed=False,
                      color=(0, 255, 0), thickness=2)

        # Update counters based on the number of detected objects within ROI
        red_detected = len(centroids_red)
        yellow_detected = len(centroids_yellow)
        green_detected = len(centroids_green)
        blue_detected = len(centroids_blue)

        # Add individual detections to the corresponding lists
        red_detections.append(red_detected)
        yellow_detections.append(yellow_detected)
        green_detections.append(green_detected)
        blue_detections.append(blue_detected)

        # Calculate the total count for each color by summing all detections
        total_red = sum(red_detections)
        total_yellow = sum(yellow_detections)
        total_green = sum(green_detections)
        total_blue = sum(blue_detections)

        # Draw the total counts on the frame for each color

        cv2.circle(frame, (30, 32), 10, (0, 0, 255), -1)
        cv2.circle(frame, (30, 72), 10, (0, 255, 255), -1)
        cv2.circle(frame, (30, 112), 10, (0, 255, 0), -1)
        cv2.circle(frame, (30, 152), 10, (255, 0, 0), -1)

        cv2.putText(frame, f': {total_red}',
                    (50, 40), font, 0.75, (255, 255, 255), 2, cv2.LINE_AA)
        cv2.putText(frame, f': {total_yellow}',
                    (50, 80), font, 0.75, (255, 255, 255), 2, cv2.LINE_AA)
        cv2.putText(frame, f': {total_green}',
                    (50, 120), font, 0.75, (255, 255, 255), 2, cv2.LINE_AA)
        cv2.putText(frame, f': {total_blue}', (50, 160),
                    font, 0.75, (255, 255, 255), 2, cv2.LINE_AA)

        total = total_red + total_yellow + total_green + total_blue
        cv2.putText(frame, f'Total: {total}', (15, 200),
                    font, 0.75, (255, 255, 255), 2, cv2.LINE_AA)

        cv2.imshow('Color Pieces Counter', frame)

        key = cv2.waitKey(1) & 0xFF
        if key == ord('s'):
            save_values_in_json()
        elif key == ord('r'):
            red_detections = []
            yellow_detections = []
            green_detections = []
            blue_detections = []
        elif key == ord('q'):
            break

    # Release camera and close all OpenCV windows on completion
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
