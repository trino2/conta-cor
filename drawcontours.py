import cv2

width, height = 640, 480
font = cv2.FONT_HERSHEY_SIMPLEX

# Función para dibujar contornos y centroides
def draw(frame, contours, color):
    global x
    for (i, c) in enumerate(contours):
        area = cv2.contourArea(c)
        if area > 8000:
            M = cv2.moments(c)
            if M["m00"] == 0:
                M["m00"] = 1
            x = int(M["m10"] / M["m00"])
            y = int(M['m01'] / M['m00'])

            newContour = cv2.convexHull(c)
            cv2.circle(frame, (x, y), 3, color, -1)
            cv2.drawContours(frame, [newContour], 0, color, 2)            
    return contours
