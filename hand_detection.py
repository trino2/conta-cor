from flask import Flask, render_template, Response, jsonify
import cv2
import mediapipe as mp
import time

app = Flask(__name__)

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mphands = mp.solutions.hands

# camera settings
cam = 3
width, height = 640, 480
fps = 60
brightness, contrast, saturation, focus = 5, 130, 112, 50
cap = cv2.VideoCapture(cam, cv2.CAP_V4L2)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
cap.set(cv2.CAP_PROP_FPS, fps)
cap.set(cv2.CAP_PROP_BRIGHTNESS, brightness)
cap.set(cv2.CAP_PROP_CONTRAST, contrast)
cap.set(cv2.CAP_PROP_SATURATION, saturation)
cap.set(cv2.CAP_PROP_FOCUS, focus)

hands = mphands.Hands()

def gen_frames():
    global fps_disp
    # FPS Teste
    start_time = time.time()
    display_time = 10
    fc = 0
    p_fps = 0

    font = cv2.FONT_HERSHEY_SIMPLEX
    while True:
        ret, frame = cap.read()
        if frame is None:
            break

        fc += 1

        TIME = time.time() - start_time

        if (TIME) >= display_time:
            p_fps = fc / (TIME)
            fc = 0
            start_time = time.time()
        fps_disp = round(p_fps, 2)

        frame = cv2.cvtColor(cv2.flip(frame,1), cv2.COLOR_RGB2BGR)
        results = hands.process(frame)
        
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        if results.multi_hand_landmarks:
            print(True)
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    frame,
                    hand_landmarks, mphands.HAND_CONNECTIONS)
       
        else:
            print(False)
        buffer = cv2.imencode('.jpg', frame)[1]
        frame = buffer.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concatena frame para mostrar el resultado
        time.sleep(0.05)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/get_data')
def get_data():
    fecha = time.strftime("%d/%m/%Y")
    hora = time.strftime("%H")
    minutos = time.strftime("%M")
    segundos = time.strftime("%S")
    fps = fps_disp

    # Devolver los datos en formato JSON
    return jsonify({
        "fecha": fecha,
        "hora": hora,
        "minutos": minutos,
        "segundos": segundos,
        "fps": fps
    })


@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=False)
