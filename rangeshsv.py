import cv2
import numpy as np

def hsv(frame, rangos_color):
    frameHsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    contourscolor = {}
    for color, rangos in rangos_color.items():
        h_min, h_max = rangos["HUE"]
        s_min, s_max = rangos["SAT"]
        v_min, v_max = rangos["VALUE"]

        low = np.array([h_min, s_min, v_min], np.uint8)
        high = np.array([h_max, s_max, v_max], np.uint8)

        mask = cv2.inRange(frameHsv, low, high)
        contours = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
        contourscolor[color] = contours
    return contourscolor



    
